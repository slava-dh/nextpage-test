$(document).ready(function(){


// <!-- magnific-popup -->
//     $('.popup-content').magnificPopup({
//             type: 'inline'
//         });


    <!-- mask-telephone -->
    jQuery(function($){
        $("#phone").mask("+38 (999) 999-99-99");
        $(".phone").mask("+38 (999) 999-99-99");
    });

<!-- menu tab bootstrap-->
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });

// for carousel
    $("#lets-talk").click(function () {
        $("#block2-title").addClass("d-none");
    });

    $("#myModal").on('hidden.bs.modal', function () {
        $("#block2-title").removeClass("d-none");
    });

    $('.ma5slider').on('ma5.activeSlide', function (event, slide) {
        $('.active-page').removeClass('active-page');
        $("li.page-item[data-target='" + slide +  "']").addClass('active-page');

        let previous_li = $("div.pagination-name>ul>li.active-process-name");
        previous_li.removeClass('active-process-name');
        // previous_li.children('hr').removeClass('d-none');
        previous_li.children('p').removeClass('d-none');
        let active_li = $("div.pagination-name>ul>li[data-target='" + slide +  "']");
        active_li.addClass('active-process-name');
        // active_li.children('hr').addClass('d-block');
        active_li.children('p').addClass('d-none');
    });

// END for carousel




// закрытие document.ready
});
